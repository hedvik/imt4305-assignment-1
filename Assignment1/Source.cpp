#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>

#include "ProcessingFunctions.h"

auto loadImage(std::string path, cv::ImreadModes readMode) {
	auto sourceImage = cv::imread(path, readMode);
	return sourceImage;
}

int main(int argc, char** argv) {
	auto sourceImage0 = loadImage("./sourceImages/image0.png", cv::ImreadModes::IMREAD_GRAYSCALE);
	auto sourceImage1 = loadImage("./sourceImages/image1.png", cv::ImreadModes::IMREAD_GRAYSCALE);

	if (sourceImage0.empty() || sourceImage1.empty())  {
		std::cout << "Could not open or find the image" << std::endl;
		return -1;
	}

	// Task 1 a)
	cv::imwrite("./processedImages/negative/negative.png", turnImageNegative(sourceImage0));

	// Task 1 b)
	// Comments: The higher gammas provide a darker image. 
	cv::imwrite("./processedImages/gammaCorrection/gammaCorrected_2_5.png", gammaCorrectImage(sourceImage0, 2.5));
	cv::imwrite("./processedImages/gammaCorrection/gammaCorrected_5.png", gammaCorrectImage(sourceImage0, 5));

	// Task 2), the function handles writing to file by itself. 
	bitPlaneSliceImage(sourceImage0);

	// Task 3), 
	// Comments: Its hard to tell which values/point coordinates I need to input for the image to actually improve in "image quality". 
	cv::imwrite("./processedImages/contrastStretching/contrastStretched.png", contrastStretchImage(sourceImage1, cv::Vec2i(80, 30), cv::Vec2i(160, 200)));

	// Task 4)
	histogramEqualization(sourceImage1);

	cv::waitKey(0);

	return 0;
}