#pragma once
#include <math.h>

const int MAX_INTENSITY = 255;

static auto turnImageNegative(const cv::Mat& image);
static auto gammaCorrectImage(const cv::Mat& image, float gamma);
static void bitPlaneSliceImage(const cv::Mat& image);
static void bitPlaneSliceImage(const cv::Mat& image);
static auto contrastStretchImage(const cv::Mat& image, cv::Vec2i point0, cv::Vec2i point1);
static void histogramEqualization(const cv::Mat& image);
static auto calculateHistogram(const cv::Mat& image);
static void displayHistogram(std::vector<int> histogram, std::string windowName, int imageWidth);

static auto turnImageNegative(const cv::Mat& image) {
	auto processedImage = image.clone();

	// Image is grayscale
	if (image.channels() == 1) {
		// Processing on a pixel by pixel basis is the easiest way, but also one of the slowest. 
		for (int i = 0; i < image.rows; i++) {
			for (int j = 0; j < image.cols; j++) {
				processedImage.at<uchar>(i, j) = MAX_INTENSITY - processedImage.at<uchar>(i, j);
			}
		}
	}

	// Image is in BGR color space
	else if (image.channels() == 3) {
		for (int i = 0; i < image.rows; i++) {
			for (int j = 0; j < image.cols; j++) {
				processedImage.at<cv::Vec3b>(i, j)[0] = MAX_INTENSITY - processedImage.at<cv::Vec3b>(i, j)[0];
				processedImage.at<cv::Vec3b>(i, j)[1] = MAX_INTENSITY - processedImage.at<cv::Vec3b>(i, j)[1];
				processedImage.at<cv::Vec3b>(i, j)[2] = MAX_INTENSITY - processedImage.at<cv::Vec3b>(i, j)[2];
			}
		}
	}

	return processedImage;
}

// I had some issues with s=cr^(1/gamma) as it would only work with darkening the image, 
// but not lightening it so I looked for other approaches for OpenCV specifically. 
static auto gammaCorrectImage(const cv::Mat& image, float gamma) {
	// https://subokita.com/2013/06/18/simple-and-fast-gamma-correction-on-opencv/
	// A more efficient approach in terms of processing using a lookup table. 
	cv::Mat lookupMatrix(1, MAX_INTENSITY + 1, CV_8UC1);
	uchar* ptr = lookupMatrix.ptr();
	for (int i = 0; i < MAX_INTENSITY + 1; i++) {
		ptr[i] = (int)(pow((double)i / MAX_INTENSITY, gamma) * MAX_INTENSITY);
	}

	cv::Mat processedImage;
	cv::LUT(image, lookupMatrix, processedImage);

	return processedImage;
}

static void bitPlaneSliceImage(const cv::Mat& image) {
	// Based on https://stackoverflow.com/questions/36408759/bit-planes-of-a-1-plane-image-in-opencv-only-work-for-1-3-of-the-image
	if (image.channels() == 1) {
		for (int i = 0; i < 8; i++) {
			// Using bitwise logic to slice the image by shifting bits. 
			cv::Mat bitPlane = (image & (1 << i));
			auto filePath = "./processedImages/bitPlaneSlicing/bit" + std::to_string(i) + ".png";
			cv::imwrite(filePath, bitPlane);
		}
	}
	else {
		std::cout << "ERROR: Tried applying bit plane splicing to a non grayscale image" << std::endl;
	}
}

static auto contrastStretchImage(const cv::Mat& image, cv::Vec2i point0, cv::Vec2i point1) {
	// Based on http://www.programming-techniques.com/2013/01/contrast-stretching-using-c-and-opencv.html
	auto processedImage = image.clone();
		
	if (image.channels() == 1) {
		for (int y = 0; y < image.rows; y++) {
			for (int x = 0; x < image.cols; x++) {
				auto intensityValue = processedImage.at<uchar>(y, x);
					
				// Applying contrast stretching
				auto stretchedIntensity = 0.0f;
				if (0 <= intensityValue && intensityValue <= point0[0]) {
					stretchedIntensity = point0[1] / point0[0] * intensityValue;
				}
				else if (point0[0] < intensityValue && intensityValue <= point1[0]) {
					stretchedIntensity = ((point1[1] - point0[1]) / (point1[0] - point0[0])) * (intensityValue - point0[0]) + point0[1];
				}
				else if (point1[0] < intensityValue && intensityValue <= MAX_INTENSITY) {
					stretchedIntensity = ((MAX_INTENSITY - point1[1]) / (MAX_INTENSITY - point1[0])) * (x - point1[0]) + point1[1];
				}

				processedImage.at<uchar>(y, x) = cv::saturate_cast<uchar>(stretchedIntensity);
			}
		}

		return processedImage;
	}
	else {
		std::cout << "ERROR: Tried applying contrast stretching to a non grayscale image" << std::endl;
	}
}

static auto calculateHistogram(const cv::Mat& image) {
	std::vector<int> intensityHistogram(MAX_INTENSITY + 1, 0);
	for (int y = 0; y < image.rows; y++) {
		for (int x = 0; x < image.cols; x++) {
			intensityHistogram[cv::saturate_cast<int>(image.at<uchar>(y, x))]++;
		}
	}

	return intensityHistogram;
}

static void histogramEqualization(const cv::Mat& image) {
	if (image.channels() == 1) {
		auto processedImage = image.clone();

		auto sourceHistogram = calculateHistogram(image);
		displayHistogram(sourceHistogram, "Source image histogram", image.rows);

		// I cannot find any good(and readable) code examples of how to implement histogram equalization so I am using OpenCVs function for this.  
		cv::equalizeHist(image, processedImage);

		auto equalizedHistogram = calculateHistogram(processedImage);
		displayHistogram(equalizedHistogram, "Equalized image histogram", image.rows);
		cv::imwrite("./processedImages/histogramEqualization/equalizedImage.png", processedImage); 
	}
	else {
		std::cout << "ERROR: Tried performing histogram equalization to a non grayscale image" << std::endl;
	}
}

static void displayHistogram(std::vector<int> histogram, std::string windowName, int imageWidth) {
	// Visualization code based on https://docs.opencv.org/2.4/doc/tutorials/imgproc/histograms/histogram_calculation/histogram_calculation.html
	int histogramSize = 256;
	int histogramWidth = 512;
	int histogramHeight = 400;
	int binWidth = std::round((double)histogramWidth / histogramSize);

	cv::Mat histogramImage(histogramHeight, histogramWidth, CV_8UC1, cv::Scalar(0, 0, 0));
	cv::normalize(histogram, histogram, 0, imageWidth, cv::NORM_MINMAX, -1, cv::Mat());

	for (int i = 1; i < histogramSize; i++) {
		cv::line(histogramImage,
			cv::Point(binWidth * (i - 1), histogramHeight - std::round(histogram[i - 1])),
			cv::Point(binWidth * (i), histogramHeight - std::round(histogram[i])),
			cv::Scalar(255, 255, 255),
			2, 8, 0);
	}

	cv::namedWindow(windowName, CV_WINDOW_AUTOSIZE);
	cv::imshow(windowName, histogramImage);
}

